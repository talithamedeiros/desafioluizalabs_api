# Desafio LuizaLabs

API Rest para gerenciar clientes e seus produtos favoritos

## Tecnologias utilizadas
* Python 3
* Flask
* MongoDB

## Getting Started
Faça um clone da aplicação em sua máquina local

``git clone https://gitlab.com/talithamedeiros/desafioluizalabs_api.git``


### Pré-requisitos
* Python3
* MongoDB

### Instalação

Verifique se você possui a versão >= 3.6.5 do python

``python3 -V``

Se não tiver, rode o comando

``sudo apt-get install python3.6``

Instale também o pip3

``sudo apt-get install python3-pip``

Python instalado, vamos instalar o ambiente virtual do Python

``pip3 install virtualenv``

Entre na pasta do projeto

``cd desafioluizalabs_api``

Crie seu ambiente virtual python

``python3 -m venv venv``

Verifique se você possui o MongoDB, se não tiver, [faça o download e instale na sua máquina](https://www.mongodb.com/download-center/community)


## Rodando a aplicação

Certifique-se de que está na pasta raiz do projeto e entre no ambiente através do comando

``source venv/bin/activate``

Instale os requirements do projeto

``pip3 install -r requirements.txt``

Agora rode o projeto

``python3 run.py runserver``

Obs 1.: Sempre que for rodar a aplicação é necessário estar no ambiente.

Obs 2.: Certifique-se que está com o serviço do MongoDB levantado.

## Rodando os testes

Certifique-se que está dentro do ambiente virtual criado (venv), de que você está com o MongoDB rodando e de que você instalou os requirements do projeto.

Agora rode o comando

``python3 -m pytest``