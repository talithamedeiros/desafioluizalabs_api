import pymongo
from pymongo import MongoClient


# Database connection
client = MongoClient('localhost', 27017)
db = client.clients_db


def dropDB():
  client.drop_database('clients_db')

