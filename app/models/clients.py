from flask import jsonify, Response
from app import app
from app.database.config import db
from app.models.ErrorHandling import ErrorHandling
clients_db = db.clients_db


"""
getClient(param, value)
Get a client by id or e-mail
params: param (attribute name or e-mail), value
return: a json with client's information, if exists.
"""
def getClient(param, value):
  client = clients_db.find_one({ param: value })

  if client:
    return client


"""
getNextSequenceValue()
Create "counters" collection for count the sequence of documents
return: the sequence value
"""
def getNextSequenceValue():
  if int(db.counters.count_documents({})) == 0:
    db.counters.insert_one({"_id": "client_id", "sequence_value": 1})
      
  sequenceDocument = db.counters.find_one_and_update({"_id": "client_id"}, {"$inc": {"sequence_value": 1}})

  return int(sequenceDocument["sequence_value"])


"""
post(name, email)
Insert a client
params: name, email
return: a message or json object and HTTP status code 200
"""
def post(name, email):
  errorHandl = ErrorHandling('', {})
  client_email = getClient('email', email)

  if not client_email:    
    clients_db.insert_one({"_id": getNextSequenceValue(), "name": name, "email": email})
    errorHandl.message = 'Client was successfully created.'
    errorHandl.object = getClient('email', email)
    return errorHandl.getReturn(), 201
  
  else:
    errorHandl.message = 'Client already exists.'
    return errorHandl.getReturn(), 200


"""
put(id, name)
Update a client
params: id, name
return: 
  - success: a json with success's message, object and HTTP status code 200
  - fail: a message and HTTP status code 404
"""
def put(id, name):
  errorHandl = ErrorHandling('', {})
  
  if id.isdigit():
    client = getClient('_id', int(id))

    if client:
      clients_db.update_one(client, { "$set": { "name": name } })
      errorHandl.message = 'Client was successfully updated.'
      errorHandl.object = getClient('_id', int(id))
      return errorHandl.getReturn(), 200

    else: 
      errorHandl.message = 'No client found.'
      return errorHandl.getReturn(), 200

  else:
    return 'Invalid param.', 404


"""
delete(id, name)
Delete a client
params: id
return: 
  - success: a json with success's message, object and HTTP status code 200
  - fail: a message and HTTP status code 404
"""
def delete(id):
  errorHandl = ErrorHandling('', {})

  if id.isdigit():
    client = getClient('_id', int(id))

    if client:
      clients_db.delete_one({"_id": int(id)})
      errorHandl.message = 'Client was successfully removed.'
      errorHandl.object = client
      return errorHandl.getReturn(), 200

    else:
      errorHandl.message = 'No client found.'
      return errorHandl.getReturn(), 200

  else:
    return 'Invalid param.', 404 


"""
get(id)
Get a client
params: id
return: 
  - success: a json with all client's information (including favorite products), if exists, and HTTP status code 200
  - fail: a message and HTTP status code 404
"""
def get(id):
  errorHandl = ErrorHandling('', {})

  if id.isdigit():
    client = getClient('_id', int(id))

    if client:
      return jsonify(client)

    else:
      errorHandl.message = 'No client found.'
      return errorHandl.getReturn(), 200

  else:
    return 'Invalid param.', 404


"""
getAll()
Get all clients
params: id
return: a json with all clients and HTTP status code 200
"""
def getAll():
  clients = clients_db.find()
  result = []

  for client in clients:
    result.append({"id": client["_id"], "name": client["name"], "email": client["email"]})

  return jsonify(result), 200