import pytest
from app import create_app
from app.database.config import dropDB
from app.models.clients import *
from app.models.products import *


@pytest.fixture
def app():
  # Initializing tests
  app = create_app()
  setup()

  # Run tests
  yield app
  
  # Finalizing tests
  teardown()


def setup():
  dropDB()
  insertDatabase()


def teardown():
  dropDB()


def insertDatabase():
  clients_db = db.clients_db

  # Insert clients
  post('Alice', 'alice@email.com')
  post('John', 'john@email.com')
  post('George', 'george@email.com')

  # Insert client's favorite products
  addProduct('1', '77be5ad3-fa87-d8a0-9433-5dbcc3152fac')
  addProduct('2', '9896cdf5-4e97-d245-8fa4-d7c9e4c773df')
  addProduct('2', '3fbe9943-2f2d-99ba-8f1b-a0263d49339f')